'use strict';

const Emitter = require('../helpers/events/event_emitter');
const config = require('../infra/configs/global_config');
const AZTS = require('../helpers/databases/azts/db');
const Dynamo = require('../helpers/databases/dynamo/db');
const azure = require('azure-storage');
const entGen = azure.TableUtilities.entityGenerator;
const wrapper = require('../helpers/utils/wrapper');
const SQS = require('../helpers/components/aws-sqs/sqs');

let statusPolling = 0;
let numberFailed = 0;

const init = async () => {
    const result = await enableStatusPolling();
    if(result.err){
        Emitter.emitEvent('error', result.err);
    }else{
        console.log(`Event Polling is Running...`);
        statusPolling = 1;
        numberFailed = 0;
        const sqs = new SQS(config.getQueueRony());
        sqs.init();
        queuePolling(sqs);
    }
}

const getStatusPolling = () => {
    return statusPolling;
}

const enableStatusPolling = async () => {
    const result = await updateTriggerPolling(true);
    return result;
}

const disableStatusPolling = async () => {
    const result = await updateTriggerPolling(false);
    return result;
}

// const updateTriggerPolling = async (status) => {
//     const entity = {
//         PartitionKey: entGen.String('statusPolling'),
//         RowKey: entGen.String('product'),
//         status: entGen.Boolean(status),
//     };
//     const azts = new AZTS(config.getAZTSPolling());
//     azts.setTable(`TriggerPolling`);
//     const result = await azts.insertOrMergeEntity(entity).then(data =>{
//         return wrapper.data(data);
//     }).catch(err =>{
//         return wrapper.error(err,err.message,503);
//     });
//     return result;
// }

const updateTriggerPolling = async (info) => {
    const table = 'TriggerPolling';
    const domain = 'product';
    const params = {
        TableName: table,
        Key:{
            'domain': domain
        },
        UpdateExpression: "set statusPolling = :sp",
        ExpressionAttributeValues:{
            ":sp":info
        },
        ReturnValues:"UPDATED_NEW"
    };

    const dynamo = new Dynamo(config.getAWSCredential());
    dynamo.init();
    const result = await dynamo.updateItem(params).then(data => {
        return wrapper.data(data);
    }).catch(err => {
        return wrapper.error(err, err.message, 503);
    })
    return result;
}

const queuePolling = async (sqs) => {
    sqs.receiveQueue(async (err,message,receipt) => {
        if(err){
            numberFailed++;
            if (numberFailed>5) {
                const result = await updateTriggerPolling(false);
                (result.err) ? Emitter.emitEvent('error', result.err) : statusPolling = 0;
            }
            Emitter.emitEvent('error', err);
        }else if(message===''){
            const result = await updateTriggerPolling(false);
            if (result.err) {
                Emitter.emitEvent('error', result.err);
            }else{
                statusPolling = 0;
                console.log(`Queue has been Empty!!!`);
            }
        }else{
            Emitter.emitEvent('queueBroker', message, receipt, sqs);
        }
        (statusPolling===1) ? queuePolling(sqs) : console.log(`Event Polling was Stopped!!!`);
    });
}

module.exports = {
    init: init,
    getStatusPolling: getStatusPolling
}
