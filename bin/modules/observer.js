'use strict';

const Emitter = require('../helpers/events/event_emitter');
const EventLogger = require('../helpers/events/event_logger');
const myEmitter = Emitter.myEmitter;
const eventPolling = require('./event_polling');
const sentryLog = require('../helpers/components/sentry/sentry_log');
const brandEventHandler = require('./brand_cqrs/handlers/event_handler');
const categoryEventHandler = require('./category_cqrs/handlers/event_handler');
const productHandler = require('./product/handlers/event_handler');
const specificationEventHandler = require('./product_specification_cqrs/handlers/event_handler');

const init = () => {
    console.log(`Observer is Running...`);
    initEventListener();
    //Emitter.emitEvent('eventPolling');
}

const executeEventPolling = () => {
    if (eventPolling.getStatusPolling()===0){
        eventPolling.init();
    }
}

const initEventListener = () => {
    myEmitter.on('tlu_InveCPBrandCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            brandEventHandler.tlu_InveCPBrandCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('tlu_InveCPBrandEventingCreated', (message) => {
        setImmediate(() => {
            brandEventHandler.tlu_InveCPBrandEventingCreatedHandler(message);
        });
    });
    myEmitter.on('tlu_InveCPBrandToEventStoreCreated', (data) => {
        setImmediate(() => {
            brandEventHandler.tlu_InveCPBrandToEventStoreCreatedHandler(data);
            // brandEventHandler.BrandToPlankton(data)
        });
    });
    myEmitter.on('tlu_InveCPBrandUpdated', (message, receipt, sqs) => {
       setImmediate(() => {
           brandEventHandler.tlu_InveCPBrandUpdatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('tlu_InveCPBrandEventingUpdated', (message) => {
        setImmediate(() => {
            brandEventHandler.tlu_InveCPBrandUpdatedtoEventStoreHandler(message)
        });
    });
    myEmitter.on('tlu_InveCPBrandToEventStoreUpdated', (data) => {
        setImmediate(() => {
            brandEventHandler.tlu_InveCPBrandUpdatedToViewHandler(data)
            // brandEventHandler.BrandToPlankton(data)
        })
    })

    myEmitter.on('tlu_InveCPCategoryCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('tlu_InveCPCategoryEventingCreated', (message) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryEventingCreatedHandler(message);
        });
    });
    myEmitter.on('tlu_InveCPCategoryToEventStoreCreated', (data) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryToEventStoreCreatedHandler(data);
        });
    });

    myEmitter.on('trx_InveCPCatRelCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveCPCatRelCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('trx_InveCPCatRelEventingCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveCPCatRelEventingCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('trx_InveCPCatRelToEventStoreCreated', (data) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveCPCatRelToEventStoreCreatedHandler(data);
        });
    });
    myEmitter.on('trx_InveTmpltDetailCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveTmpltDetailCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('trx_InveTmpltDetailEventingCreated', (message,receipt,sqs) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveTmpltDetailEventingCreatedHandler(message,receipt,sqs);
        });
    });
    myEmitter.on('trx_InveTmpltDetailToEventStoreCreated', (data) => {
        setImmediate(() => {
            specificationEventHandler.trx_InveTmpltDetailToEventStoreCreatedHandler(data);
        });
    });
    myEmitter.on('tlu_InveCPCategoryUpdated', (message, receipt, sqs) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryUpdatedHandler(message)
        })
    });
    myEmitter.on('tlu_InveCPCategoryEventingUpdated',(message) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryEventingUpdatedHandler(message)
        })
    })
    myEmitter.on('tlu_InveCPCategoryToEventStoreUpdated', (data) => {
        setImmediate(() => {
            categoryEventHandler.tlu_InveCPCategoryToEventStoreUpdatedHandler(data)
        })
    })
    myEmitter.on('trx_InveComputerV3Created', (message,receipt,sqs) => {
       setImmediate(() => {
            productHandler.trx_InveComputerCreatedHandler(message,receipt,sqs);
       });
    });
    myEmitter.on('trx_InveComputerV4Created', (message,receipt,sqs) => {
       setImmediate(() => {
            productHandler.trx_InveComputerCreatedHandler(message,receipt,sqs);
       });
    });
    myEmitter.on('trx_InveComputerEventingCreated', (message) => {
        setImmediate(() => {
            productHandler.trx_InveComputerEventingCreatedHandler(message);
        });
    });
    myEmitter.on('trx_InveComputerToEventStoreCreated', (data) => {
        setImmediate(() => {
            productHandler.trx_InveComputerToEventStoreCreatedHandler(data);
            productHandler.ProductListShark(data);
            productHandler.ProductDetailShark(data);
            productHandler.ProductListCrab(data);
        });
    });
    myEmitter.on('productListSharkCreated', (message) => {
        setImmediate(() => {
            productHandler.ProductListShark(data);
        });
    });
    myEmitter.on('productDetailSharkCreated', (message) => {
        setImmediate(() => {
            productHandler.ProductDetailShark(data);
        });
    });
    myEmitter.on('productListCrabCreated', (message) => {
        setImmediate(() => {
            productHandler.ProductListCrab(data);
        });
    });
    myEmitter.on('trx_InveComputerUpdated', (message,receipt,sqs) => {
        setImmediate(() => {
            productHandler.trx_InveComputerUpdatedHandler(message,receipt,sqs)
        })
    })
    myEmitter.on('trx_InveComputerEventingUpdated', (message) => {
        setImmediate(() => {
            productHandler.trx_InveComputerEventingUpdatedHandler(message)
        })
    })
    myEmitter.on('trx_InveComputerToEventStoreUpdated', (data) => {
        setImmediate(() => {
            productHandler.trx_InveComputerToEventStoreUpdatedHandler(data)
        })
    })
    myEmitter.on('eventPolling', () => {
        setImmediate(() => {
            executeEventPolling();
        });
    });
    myEmitter.on('eventHandled', (receipt,sqs) => {
        setImmediate(() => {
            sqs.removeQueue(receipt);
        });
    });
    myEmitter.on('eventLogger', (eventType,payload,aggregateType,description,priority=1,status=1) => {
        const data = {
                        "eventType": eventType,
                        "payload": payload,
                        "aggregateType": aggregateType,
                        "description": description,
                        "priority": priority,
                        "status": status
                     }
        setImmediate(async () => {
            const logger = new EventLogger(data);
            const result = await logger.createLog();
            (result.err) ? Emitter.emitEvent('error', `failed to create log`) : console.log(`Log Has Been Created`); 
        });
    });
    myEmitter.on('error', (err) => {
        console.error(`Error : ${err}`);
    });
    myEmitter.on('sendErrorToSentry', (errMessage) => {
        console.log('error Sentry');
        console.log(errMessage);
        //sentryLog.sendError(errMessage);
    });
    myEmitter.on('existed', (data) => {
        console.error(`Data has been existed : ${data}`);
    });
    process.on('uncaughtException', (err) => {
        console.log('whoops! There was an uncaught error', err);
        process.exit(1);
    });
    myEmitter.on('queueBroker', (message,receipt,sqs) => {
        let flag = false;
        setImmediate(() => {
            const msg = JSON.parse(message);
            eventList.map((value) => {
                if(msg.eventType===value){  
                    flag = true;
                }
            });
            (!flag) ? Emitter.emitEvent('eventHandled',receipt,sqs) : Emitter.emitEvent(msg.eventType, msg, receipt, sqs);
        });
    });
    const eventList = myEmitter.eventNames();
}

module.exports = {
    init: init
}
