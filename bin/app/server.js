'use strict';

const restify = require('restify');
const serveStatic = require('serve-static-restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const wrapper = require('../helpers/utils/wrapper');
//const ssoHandler = require('../modules/sso/handlers/api_handler');
const passport = require('passport-google');

const AppServer = function(){
    this.server = restify.createServer({
        name: project.name + '-server',
        version: project.version
    });

    this.server.serverKey = '';
    this.server.use(restify.plugins.acceptParser(this.server.acceptable));
    this.server.use(restify.plugins.queryParser());
    this.server.use(restify.plugins.bodyParser());
    this.server.use(restify.plugins.authorizationParser());

    //required for basic auth
    this.server.use(basicAuth.init());

    //anonymous can access the end point, place code bellow
    this.server.get('/', basicAuth.isAuthenticated, (req, res, next) => {
       wrapper.response(res,`success`,wrapper.data(`Domain Identity Management`),`This service is running properly`);
    });
    //this.server.get('/token', basicAuth.isAuthenticated, ssoHandler.getToken);
}
 
module.exports = AppServer;