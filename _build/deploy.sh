aws ecs register-task-definition --family squid-task --cli-input-json file://$WORKSPACE/_build/naru-backend-poi.json > $WORKSPACE/_build/naru-backend-poi_output.json
export PRODUCT_TASK_ARN=$(jq '.taskDefinition.taskDefinitionArn' naru-backend-poi_output.json | sed -e 's/^"//' -e 's/"$//')
export PRODUCT_TASK_FAMILY=$(jq '.taskDefinition.family' naru-backend-poi_output.json | sed -e 's/^"//' -e 's/"$//')
export PRODUCT_TASK_REVISION=$(jq '.taskDefinition.revision' naru-backend-poi_output.json)
aws ecs update-service --cluster naru-cluster --service naru-backend-poi --task-definition $PRODUCT_TASK_FAMILY:$PRODUCT_TASK_REVISION
